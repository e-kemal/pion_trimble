module trimble(
	input clk_50M,
	input rx,
	input btn0,
	input sw0,
	input sw1,
	input clk_10M,
	input pps,
	input test_mode,
	input UTCcorrect,
	output reg out_clk,
	output [6:0] hex0=7'b0,
	output [6:0] hex1=7'b0,
	output [6:0] hex2=7'b0,
	output [6:0] hex3=7'b0,
	output [6:0] hex4=7'b0,
	output [6:0] hex5=7'b0,
	output [6:0] hex6=7'b0,
	output [6:0] hex7=7'b0,
	output data_ready,
	output bad_data,
	output packet_ready,
	output test_mode_led,
	output reg pps_control,
//	output spps,
	output ready,
	output antenna,
	output satellites,
	output almanac,
	output not_disciplining_led,
	output reg out_clk_control
);

parameter bufsize=72;

reg [9:0] counter;
wire [7:0] data;
wire [bufsize*8-1:0] buf0;
reg [7:0] hour;
reg [7:0] min;
reg [7:0] sec;
reg [7:0] sec2;
reg [7:0] secstart=8'd59;
reg [7:0] day;
reg [7:0] month;
reg [15:0] year;
reg [15:0] UTCoffset;
reg [13:0] bad_counter=14'd0;
reg [13:0] good_counter=14'd0;
wire [6:0] buf_cnt;

reg [24:0] cnt1;
reg clk1;
always @(posedge clk_50M)
begin
	cnt1<=(cnt1<25'd50000000)?cnt1+25'd1:25'd0;
	clk1<=(cnt1<25'd50000000)?1'b0:1'b1;
end

always @(posedge clk1)
	if(btn0==1'b0)
		secstart<=(secstart<8'd59)?secstart+8'd1:8'd0;

always @(posedge clk_50M)
	counter<=(counter<10'd651)?counter+10'd1:10'd0;
//assign uart_clk= ~(|counter);
assign uart_clk=(counter==0)?1'b1:1'b0;

uart_rx the_uart_rx0(
	uart_clk,
	rx,
	data[7:0],
	data_ready,
	bad_data
);

tsip_rx #(bufsize) the_tsip_rx0(
	data_ready,
	data[7:0],
	buf0[bufsize*8-1:0],
	buf_cnt,
	packet_ready
);

reg [7:0] debug0;
reg [7:0] debug1;
indication(
	hex0, hex1, hex2, hex3, hex4, hex5, hex6, hex7,
	sw0, sw1,
	hour, min, sec, sec2,
	day, month, year,
	good_counter,
	bad_counter,
	data,
	buf0[0*8+7:0*8+0],
//	buf0[1*8+7:1*8+0],
//	buf0[2*8+7:2*8+0]
	debug0,
//	debug1
	secstart
);

always @(posedge bad_data)
	bad_counter<=bad_counter+14'd1;

always @(posedge packet_ready)
begin
	good_counter<=good_counter+14'd1;
	if((buf_cnt==7'd18) && (buf0[17*8+7:17*8+0]==8'h8f) && (buf0[16*8+7:16*8+0]==8'hab))
	begin
		year<={buf0[1*8+7:1*8+0],buf0[0*8+7:0*8+0]};
		month<=buf0[2*8+7:2*8+0];
		day<=buf0[3*8+7:3*8+0];
		hour<=buf0[4*8+7:4*8+0];
		min<=buf0[5*8+7:5*8+0];
		sec<=buf0[6*8+7:6*8+0];
		UTCoffset={buf0[9*8+7:9*8+0],buf0[8*8+7:8*8+0]};
		sec2=(UTCcorrect?{buf0[15*8+7:15*8+0],buf0[14*8+7:14*8+0],buf0[13*8+7:13*8+0],buf0[12*8+7:12*8+0]}-UTCoffset:{buf0[15*8+7:15*8+0],buf0[14*8+7:14*8+0],buf0[13*8+7:13*8+0],buf0[12*8+7:12*8+0]})%60;

//		leds<=buf0[7*8+7:7*8+0];
//		ready_start<=(buf0[6*8+7:6*8+0]==8'd59)?1'b1:1'b0;
		ready_start<=(sec2==/*8'd59*/secstart)?1'b1:1'b0;
	end
	else
		ready_start<=0;

	if((buf_cnt==7'd69) && (buf0[68*8+7:68*8+0]==8'h8f) && (buf0[67*8+7:67*8+0]==8'hac))
	begin
		//=!buf0[65*8+7:65*8+0];
		//=!buf0[65*8+4];
		debug0<=buf0[57*8+7:57*8+0];
		debug1<=buf0[56*8+7:56*8+0];
		antenna_start<=!(buf0[56*8+1] | buf0[56*8+2]);
		satellites_start<=!buf0[56*8+3];
		disciplining_start<=!buf0[56*8+4];
		almanac_start<=!buf0[57*8+3];
	end
	else
	begin
		antenna_start<=0;
		satellites_start<=0;
		disciplining_start<=0;
		almanac_start<=0;
	end
end





reg ready_start;
//reg time_counter_start_pre;
trigsec #(50000000)(
	clk_50M,
	ready_start,
	ready
);

reg antenna_start;
trigsec #(50000000)(
	clk_50M,
	antenna_start,
	antenna
);

reg satellites_start;
trigsec #(50000000)(
	clk_50M,
	satellites_start,
	satellites
);

reg disciplining_start;
trigsec #(50000000)(
	clk_50M,
	disciplining_start,
	disciplining
);
assign not_disciplining_led=!disciplining;

reg almanac_start;
trigsec #(50000000)(
	clk_50M,
	almanac_start,
	almanac
);

//wire spps;
sync synk_pps(
	.clk(clk_10M), 
	.D(pps),
	.Q(spps)
);

//wire ready;
//sync(
//	.clk(clk_10M), 
//	.D(led8),
//	.Q(ready)
//);

assign test_mode_led=test_mode;

reg spps_pre;
reg [31:0] maincounter;
always @(posedge clk_10M)
begin
	spps_pre<=pps;
	if(ready & pps & !spps_pre & (disciplining | test_mode_led))
	begin
		maincounter<=1;
		out_clk<=1;
	end
	else
	begin
		if(maincounter>=200000000)
		begin
			maincounter<=0;
		end
		else
		begin
			if(|maincounter)
			begin	
				maincounter<=maincounter+1;
				out_clk<=((maincounter%500000)<1000)?1'b1:1'b0;
			end
			else
				out_clk<=0;
		end
	end
end


/*
wire start=pps && led8;
always @(posedge clk_10M, posedge start)
begin
	if(start)maincounter<=0;
	else maincounter<=maincounter+1;
//	out_clk<=((maincounter%500000==0) /*&& (maincounter<100000000)* /)?1:0;
end
assign out_clk=((maincounter%500000==18'd0) /*&& (maincounter<100000000)* /)?1:0;
*/

/*
always @(posedge clk_50M)//, posedge time_counter_start)
begin
	time_counter_start_pre<=time_counter_start;
	if(time_counter_start & !time_counter_start_pre)
		time_counter<=50000000;
	else
	if(time_counter>0)
		time_counter<=time_counter-1;
//	if(time_counter_start & !time_counter_start_pre)
//		time_counter<=0;
//	else
//	if(time_counter<50000000)
//		time_counter<=time_counter+1;
		
		
		
		
//	if(time_counter_start)time_counter<=25000000;
//	else
//		if(time_counter<25000000)time_counter<=time_counter+1;
//		if(time_counter>0)time_counter<=time_counter-1;
end*/

//assign led8=|time_counter;
//assign led8=(time_counter<50000000)?1:0;//(time_counter<25000000-1)?1:0;


always @(posedge data_ready)
begin
//	good_counter<=good_counter+14'd1;
end

always @(posedge pps)
	pps_control<=!pps_control;

always @(posedge out_clk)
	out_clk_control<=!out_clk_control;

endmodule