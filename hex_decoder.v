module hex_decoder(
	input [4:0] in,
	output [6:0] out
);

assign out[0]=(in[4]==1'd0 && ((in[3:0]==4'h0) || (in[3:0]==4'h2) || (in[3:0]==4'h3) || (in[3:0]==4'h5) || (in[3:0]==4'h6) || (in[3:0]==4'h7) || (in[3:0]==4'h8) || (in[3:0]==4'h9) || (in[3:0]==4'ha) || (in[3:0]==4'he) || (in[3:0]==4'hf)))?1'b0:1'b1;
assign out[1]=(in[4]==1'd0 && ((in[3:0]==4'h0) || (in[3:0]==4'h1) || (in[3:0]==4'h2) || (in[3:0]==4'h3) || (in[3:0]==4'h4) || (in[3:0]==4'h7) || (in[3:0]==4'h8) || (in[3:0]==4'h9) || (in[3:0]==4'ha) || (in[3:0]==4'hd)))?1'b0:1'b1;
assign out[2]=(in[4]==1'd0 && ((in[3:0]==4'h0) || (in[3:0]==4'h1) || (in[3:0]==4'h3) || (in[3:0]==4'h4) || (in[3:0]==4'h5) || (in[3:0]==4'h6) || (in[3:0]==4'h7) || (in[3:0]==4'h8) || (in[3:0]==4'h9) || (in[3:0]==4'ha) || (in[3:0]==4'hb) || (in[3:0]==4'hd)))?1'b0:1'b1;
assign out[3]=(in[4]==1'd0 && ((in[3:0]==4'h0) || (in[3:0]==4'h2) || (in[3:0]==4'h3) || (in[3:0]==4'h5) || (in[3:0]==4'h6) || (in[3:0]==4'h8) || (in[3:0]==4'h9) || (in[3:0]==4'hb) || (in[3:0]==4'hc) || (in[3:0]==4'hd) || (in[3:0]==4'he)))?1'b0:1'b1;
assign out[4]=(in[4]==1'd0 && ((in[3:0]==4'h0) || (in[3:0]==4'h2) || (in[3:0]==4'h6) || (in[3:0]==4'h8) || (in[3:0]==4'ha) || (in[3:0]==4'hb) || (in[3:0]==4'hc) || (in[3:0]==4'hd) || (in[3:0]==4'he) || (in[3:0]==4'hf)))?1'b0:1'b1;
assign out[5]=(in[4]==1'd0 && ((in[3:0]==4'h0) || (in[3:0]==4'h4) || (in[3:0]==4'h5) || (in[3:0]==4'h6) || (in[3:0]==4'h8) || (in[3:0]==4'h9) || (in[3:0]==4'ha) || (in[3:0]==4'hb) || (in[3:0]==4'he) || (in[3:0]==4'hf)))?1'b0:1'b1;
assign out[6]=(in[4]==1'd0 && ((in[3:0]==4'h2) || (in[3:0]==4'h3) || (in[3:0]==4'h4) || (in[3:0]==4'h5) || (in[3:0]==4'h6) || (in[3:0]==4'h8) || (in[3:0]==4'h9) || (in[3:0]==4'ha) || (in[3:0]==4'hb) || (in[3:0]==4'hc) || (in[3:0]==4'hd) || (in[3:0]==4'he) || (in[3:0]==4'hf)))?1'b0:1'b1;

endmodule