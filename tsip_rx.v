module tsip_rx
#(parameter bufsize=10)
(
	input clk,
	input [7:0] data,
	output reg [bufsize*8-1:0] buf0,
	output reg [6:0] buf_cnt,
	output reg packet_ready
);

//parameter bufsize=72;

reg [1:0] reciever_state=2'd0;

always @(posedge clk)
begin	
	case(reciever_state)
		0:if(data==8'h10)
			begin
				reciever_state<=1;
				buf_cnt<=0;
			end
		1:if(data==8'h10)
			begin
				reciever_state<=2;
			end
			else
			if(data==8'h03 && buf_cnt==7'd0)
			begin
				reciever_state<=0;//Не пакет это!
			end
			else
			begin
				buf_cnt<=buf_cnt+7'd1;
			end
		2:if(data==8'h10)
			begin
				buf_cnt<=buf_cnt+7'd1;
				reciever_state<=1;
			end
			else
			begin
				reciever_state<=0;
//				buf_cnt<=0;
			end
	endcase
	if((reciever_state==2'd1 && data!=8'h10) || (reciever_state==2'd2 && data==8'h10))
		buf0<={buf0[8*(bufsize-1)-1:0], data};
	packet_ready<=(reciever_state==2'd2 && data==8'h03 && buf_cnt>0)?1'b1:1'b0;
end

endmodule