module indication(
	output [6:0] hex0=7'b0,
	output [6:0] hex1=7'b0,
	output [6:0] hex2=7'b0,
	output [6:0] hex3=7'b0,
	output [6:0] hex4=7'b0,
	output [6:0] hex5=7'b0,
	output [6:0] hex6=7'b0,
	output [6:0] hex7=7'b0,
	input sw0,
	input sw1,
	input [7:0] hour,
	input [7:0] min,
	input [7:0] sec,
	input [7:0] sec2,
	input [7:0] day,
	input [7:0] month,
	input [15:0] year,
	input [13:0] bad_counter,
	input [13:0] good_counter,
	input [7:0] dump0,
	input [7:0] dump1,
	input [7:0] dump2,
//	input [7:0] dump3
	input [7:0] secstart
);

wire [7:0] sec3=(secstart==8'd59)?8'd0:secstart+8'd1;

hex_decoder decoder0(bcd0, hex0);
hex_decoder decoder1(bcd1, hex1);
hex_decoder decoder2(bcd2, hex2);
hex_decoder decoder3(bcd3, hex3);
hex_decoder decoder4(bcd4, hex4);
hex_decoder decoder5(bcd5, hex5);
hex_decoder decoder6(bcd6, hex6);
hex_decoder decoder7(bcd7, hex7);

wire [4:0] good_counter_bcd7={1'b0, good_counter/1000};
wire [4:0] good_counter_bcd6={1'b0, good_counter/100%10};
wire [4:0] good_counter_bcd5={1'b0, good_counter/10%10};
wire [4:0] good_counter_bcd4={1'b0, good_counter%10};
wire [4:0] bad_counter_bcd3={1'b0, bad_counter/1000};
wire [4:0] bad_counter_bcd2={1'b0, bad_counter/100%10};
wire [4:0] bad_counter_bcd1={1'b0, bad_counter/10%10};
wire [4:0] bad_counter_bcd0={1'b0, bad_counter%10};

wire [4:0] debug_bcd7={1'b0, sec3/10};
wire [4:0] debug_bcd6={1'b0, sec3%10};
wire [4:0] debug_bcd5={1'b0, dump2[7:4]};
wire [4:0] debug_bcd4={1'b0, dump2[3:0]};
wire [4:0] debug_bcd3={1'b0, dump1[7:4]};
wire [4:0] debug_bcd2={1'b0, dump1[3:0]};
wire [4:0] debug_bcd1={1'b0, dump0[7:4]};
wire [4:0] debug_bcd0={1'b0, dump0[3:0]};


wire [4:0] time_bcd7={1'b0, (hour/8'd10)};
wire [4:0] time_bcd6={1'b0, (hour%10)};
				//min
wire [4:0] time_bcd5={1'b0, min/10};
wire [4:0] time_bcd4={1'b0, min%10};
				//sec
wire [4:0] time_bcd3={1'b0, sec/10};
wire [4:0] time_bcd2={1'b0, sec%10};
wire [4:0] time_bcd1={1'b0, sec2/10};
wire [4:0] time_bcd0={1'b0, sec2%10};

wire [4:0] date_bcd7={1'b0, day/10};
wire [4:0] date_bcd6={1'b0, day%10};
				//month
wire [4:0] date_bcd5={1'b0, month/10};
wire [4:0] date_bcd4={1'b0, month%10};
				//year
wire [4:0] date_bcd3={1'b0, year/1000};
wire [4:0] date_bcd2={1'b0, year/100%10};
wire [4:0] date_bcd1={1'b0, year/10%10};
wire [4:0] date_bcd0={1'b0, year%10};

wire [4:0] bcd7=sw1?sw0?good_counter_bcd7:debug_bcd7:sw0?time_bcd7:date_bcd7;
wire [4:0] bcd6=sw1?sw0?good_counter_bcd6:debug_bcd6:sw0?time_bcd6:date_bcd6;
wire [4:0] bcd5=sw1?sw0?good_counter_bcd5:debug_bcd5:sw0?time_bcd5:date_bcd5;
wire [4:0] bcd4=sw1?sw0?good_counter_bcd4:debug_bcd4:sw0?time_bcd4:date_bcd4;
wire [4:0] bcd3=sw1?sw0?bad_counter_bcd3:debug_bcd3:sw0?time_bcd3:date_bcd3;
wire [4:0] bcd2=sw1?sw0?bad_counter_bcd2:debug_bcd2:sw0?time_bcd2:date_bcd2;
wire [4:0] bcd1=sw1?sw0?bad_counter_bcd1:debug_bcd1:sw0?time_bcd1:date_bcd1;
wire [4:0] bcd0=sw1?sw0?bad_counter_bcd0:debug_bcd0:sw0?time_bcd0:date_bcd0;

endmodule