module uart_rx (clk, rx, data, data_ready, bad_data);

input	wire		clk;
input	wire		rx;
output	reg	[7:0]	data;//+1 stop bit
output				data_ready;
output				bad_data;

reg rx_ff;
reg rx_ff2; // две защелки, для вычленения старта передачи 
                    // и вообще, защелки надо, чтобы всякие помехи не ловить

always @(posedge clk)
begin
	rx_ff <= rx;
	rx_ff2 <= rx_ff;
end

// отлавливаем старт-бит
wire spad = ~rx_ff & rx_ff2;
// состояние приемника
reg receive;
// для корректной симуляции. Насколько знаю, квартус это пропускает
initial receive = 0;

//Ну тут понятно - если старт бит, то включаем режим приема,
//если приняли - выключаем
always @(posedge clk)
	if (spad) 
		receive <= 1'b1;
	else
//		if (count_byte == 10)//(data_ready)
		if(count_byte>7 && (data_ready || bad_data))
			receive <= 1'b0;
//cигнал начала приема. Для инициализации счетчиков.
wire start = ~receive & spad;

//поскольку у нас clk в 8 раз быстрее rx, делаем делитель
reg [2:0] count_os;

always @(posedge clk)
	if (start)
		count_os <= 3'd5;//чтобы попасть в середину бита
	else
		if(receive)
			count_os <= count_os + 1'b1;
//при переполнении счетчика-делителя выдираем бит из входных данных
wire get_bit = ~|count_os;
//счетчик принятых данных. Как примем 9 бит - можно останавливаться
reg [4:0] count_byte;
always @(posedge get_bit or posedge start)
begin
	if (start)
		count_byte <= 0;
	else
		count_byte <= count_byte + 4'b1;
end

reg data_ready;
reg bad_data;
//wire data_ready = (count_byte == 9) && rx_ff==1;//приняли все биты и крайний (стоповый) бит равен 1
//сдвигаем регистр данных на одну позицию вправо,
//и пишем принятый бит в старший бит
always @(negedge get_bit)
begin
//	if(count_byte==1 && ~rx_ff)//no start bit
	if(count_byte>1 && count_byte<10) data <= {rx_ff, data[7:1]};
	data_ready = (count_byte == 5'd10) && rx_ff==1;//приняли все биты и крайний (стоповый) бит равен 1
	bad_data = (count_byte == 5'd10) && rx_ff==0;//приняли все биты и крайний (стоповый) бит НЕ равен 1
end

endmodule